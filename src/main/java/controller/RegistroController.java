/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import root.uni02.camila.gonzalez.dao.AlumnosJpaController;
import root.uni02.camila.gonzalez.dao.exceptions.NonexistentEntityException;
import root.uni02.camila.gonzalez.entity.Alumnos;

/**
 *
 * @author camil
 */
@WebServlet(name = "RegistroController", urlPatterns = {"/RegistroController"})
public class RegistroController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet RegistroController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet RegistroController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        AlumnosJpaController dao = new AlumnosJpaController();
        String opcionSeleccionada = request.getParameter("accion");
        String rutAlumnoSeleccionado = request.getParameter("alumnoSeleccionado");
        String rut, nombre, apellido, direccion, telefono;

        List<Alumnos> listaAlumnos = null;

        if (opcionSeleccionada.equals("registrar")) {
            try {
                rut = request.getParameter("rut");
                nombre = request.getParameter("nombre");
                apellido = request.getParameter("apellido");
                direccion = request.getParameter("direccion");
                telefono = request.getParameter("telefono");

                Alumnos alumnos = new Alumnos();

                alumnos.setRut(rut);
                alumnos.setNombre(nombre);
                alumnos.setApellido(apellido);
                alumnos.setDireccion(direccion);
                alumnos.setTelefono(telefono);

                // Create
                dao.create(alumnos);

            } catch (Exception ex) {
                Logger.getLogger(RegistroController.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            listaAlumnos = dao.findAlumnosEntities();
            request.setAttribute("listaAlumnos", listaAlumnos);
            request.getRequestDispatcher("lista.jsp").forward(request, response);
        }
        
        if (opcionSeleccionada.equals("consultar")) {
            listaAlumnos = dao.findAlumnosEntities();
            request.setAttribute("listaAlumnos", listaAlumnos);
            request.getRequestDispatcher("lista.jsp").forward(request, response);
        }
        
        if (opcionSeleccionada.equals("eliminar")) {
            try {
                // Destroy
                dao.destroy(rutAlumnoSeleccionado);
            } catch (NonexistentEntityException ex) {
                Logger.getLogger(RegistroController.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            listaAlumnos = dao.findAlumnosEntities();
            
            request.setAttribute("listaAlumnos", listaAlumnos);
            request.getRequestDispatcher("lista.jsp").forward(request, response);
        }
        
        if (opcionSeleccionada.equals("editar")) {
            Alumnos alumno = new Alumnos();
            alumno = dao.findAlumnos(rutAlumnoSeleccionado);            
            request.setAttribute("alumno", alumno);
            request.getRequestDispatcher("editor.jsp").forward(request, response);
        }
        
        if (opcionSeleccionada.equals("actualizar")) {
            try {
                rut = request.getParameter("rut");
                nombre = request.getParameter("nombre");
                apellido = request.getParameter("apellido");
                direccion = request.getParameter("direccion");
                telefono = request.getParameter("telefono");

                Alumnos alumnos = new Alumnos();

                alumnos.setRut(rut);
                alumnos.setNombre(nombre);
                alumnos.setApellido(apellido);
                alumnos.setDireccion(direccion);
                alumnos.setTelefono(telefono);

                // Edit
                dao.edit(alumnos);

            } catch (Exception ex) {
                Logger.getLogger(RegistroController.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            listaAlumnos = dao.findAlumnosEntities();
            request.setAttribute("listaAlumnos", listaAlumnos);
            request.getRequestDispatcher("lista.jsp").forward(request, response);
        }

        if (opcionSeleccionada.equals("irRegistro")) {
            request.getRequestDispatcher("index.jsp").forward(request, response);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
