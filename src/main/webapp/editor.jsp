<%-- 
    Document   : editor
    Created on : 25-04-2021, 22:18:11
    Author     : cGoGo
--%>

<%@page import="root.uni02.camila.gonzalez.entity.Alumnos"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    Alumnos alumno = (Alumnos) request.getAttribute("alumno");
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <p>UNI 02 Camila Gonzalez</p>
        <h1>Editar alumno</h1>
        <form name="form" action="RegistroController" method="POST">
            <label>Rut</label>
            <input type="text" name="rut" id="rut" placeholder="Ingrese Rut" value="<%= alumno.getRut() %>"><br>
            <label>Nombre</label>
            <input type="text" name="nombre" id="nombre" placeholder="Ingrese Nombre" value="<%= alumno.getNombre() %>"><br>
            <label>Apellido</label>
            <input type="text" name="apellido" id="apellido" placeholder="Ingrese Apellido" value="<%= alumno.getApellido() %>"><br>
            <label>Dirección</label>
            <input type="text" name="direccion" id="direccion" placeholder="Ingrese Dirección" value="<%= alumno.getDireccion() %>"><br>
            <label>Teléfono</label>
            <input type="text" name="telefono" id="telefono" placeholder="Ingrese Teléfono" value="<%= alumno.getTelefono() %>"><br>
            <button type="submit" name="accion" value="actualizar">Actualizar información del alumno</button><br><br>
        </form>
    </body>
</html>

