<%-- 
    Document   : index
    Created on : 25-04-2021, 19:11:03
    Author     : cGoGo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <p>UNI 02 Camila Gonzalez</p>
        <h1>Registro de Alumnos</h1>
        <form name="form" action="RegistroController" method="POST">
            <label>Rut</label>
            <input type="text" name="rut" id="rut" placeholder="Ingrese Rut"><br>
            <label>Nombre</label>
            <input type="text" name="nombre" id="nombre" placeholder="Ingrese Nombre"><br>
            <label>Apellido</label>
            <input type="text" name="apellido" id="apellido" placeholder="Ingrese Apellido"><br>
            <label>Dirección</label>
            <input type="text" name="direccion" id="direccion" placeholder="Ingrese Dirección"><br>
            <label>Teléfono</label>
            <input type="text" name="telefono" id="telefono" placeholder="Ingrese Teléfono"><br>
            <button type="submit" name="accion" value="registrar">Registrar Alumno</button><br><br>
            <button type="submit" name="accion" value="consultar">Ver lista de alumnos</button>
        </form>
    </body>
</html>
