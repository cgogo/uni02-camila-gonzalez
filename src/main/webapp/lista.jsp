<%-- 
    Document   : lista
    Created on : 25-04-2021, 19:49:12
    Author     : camil
--%>

<%@page import="java.util.*"%>
<%@page import="root.uni02.camila.gonzalez.entity.Alumnos"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    List<Alumnos> alumnos = (List<Alumnos>) request.getAttribute("listaAlumnos");
    Iterator<Alumnos> itAlumnos = alumnos.iterator();
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <p>UNI 02 Camila Gonzalez</p>
        <h1>Lista de Alumnos</h1>
        <form name="form" action="RegistroController" method="POST">
            <table border="1">
                <thead>
                <th>Rut</th>
                <th>Nombre</th>
                <th>Apellido</th>
                <th>Dirección</th>
                <th>Teléfono</th>
                <th>Seleccione</th>
                </thead>
                <tbody>
                    <%
                        while (itAlumnos.hasNext()) {
                            Alumnos alumn = itAlumnos.next();
                    %>
                    <tr>
                        <td><%= alumn.getRut() %></td>
                        <td><%= alumn.getNombre() %></td>
                        <td><%= alumn.getApellido() %></td>
                        <td><%= alumn.getDireccion() %></td>
                        <td><%= alumn.getTelefono() %></td>
                        <td>
                            <input type="radio" name="alumnoSeleccionado" value="<%= alumn.getRut()%>">
                        </td>
                    </tr>
                    <% }%>
                </tbody>
            </table>
            <button type="submit" name="accion" value="editar">Editar</button>
            <button type="submit" name="accion" value="eliminar">Eliminar</button>
            <button type="submit" name="accion" value="irRegistro">Ver formulario de registro</button>
        </form>
    </body>
</html>
